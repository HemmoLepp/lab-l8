const chai = require('chai');
const expect = chai.expect;
const request = require("request");
const {options} = require('../src/server');
const app = require("../src/server");
const port = 3000;

let server;

const arequest = async (value) => new Promise((resolve, reject) => {
    request(value, (error, response) => {
        if(error) reject(error)
        else resolve(response);
    });
});

describe("Test REST API", () => {
    before("Start server", () => {
        server = app.listen(port);
    });
    it("GET / returns 200, which means server OK", async () => {
        const res = await arequest("http://localhost:3000/");
        expect(res.statusCode).to.equal(200);
    })
    it("GET /add?a=1&b=2 returns 3", async () => {
        const options = {
            method: 'GET',
            url: 'http://localhost:3000/add',
            qs: {a: '1', b: '2'},
            headers: {'user-agent': 'vscode-restclient'}
          };
        await arequest(options)
            .then((res) => {
                expect(res.body).to.equal("3");
            })
            .catch((res) => {
                expect(true).to.equal(false, "add function failed")
            })
    })
    after("Close server", () => {
        server.close();
    });
});