const express = require('express');
const port = 3000;
const app = express();

app.get("/", (req, res) => res.send("welcome"));

// localhost:3000/add?a=2&b=3
app.get("/add", (req, res) => {
    try {
        const sum = parseInt(req.query.a) + parseInt(req.query.b)
        res.send(sum.toString());
    } catch (e){
        res.sendStatus(500);
    }
});

if (process.env.NODE_ENV === 'test'){
    module.exports = app;
}

if (!module.parent) {
    app.listen(port, () => console.log(`Server listening at: localhost:${port}`))
}

